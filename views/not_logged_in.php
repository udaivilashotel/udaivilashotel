<?php
// show potential errors / feedback (from login object)
if (isset($login)) {
    if ($login->errors) {
        foreach ($login->errors as $error) {
            echo $error;
        }
    }
    if ($login->messages) {
        foreach ($login->messages as $message) {
            echo $message;
        }
    }
}
?>

<link rel="stylesheet" href="css/style.css">

<!-- login form box -->
<form method="post" action="index.php" class="login">
    <p>
      <label for="login_username">Username:</label>
      <input type="text" name="user_name" id="login_username" placeholder="username">
    </p>

    <p>
      <label for="password">Password:</label>
      <input type="password" id="password" name="user_password" placeholder="password" autocomplete="off" required>
    </p>

    <p class="login-submit">
        <!--<input type="submit" class="login-button" name="login" value="Log in" />-->
        <button type="submit" name="login" class="login-button">Login</button>
    </p>
    <a href="views/register.php">Register new account</a>

  </form>

