<?php
// show potential errors / feedback (from registration object)
if (isset($registration)) {
	if ($registration->errors) {
		foreach ($registration->errors as $error) {
			echo $error;
		}
	}
	if ($registration->messages) {
		foreach ($registration->messages as $message) {
			echo $message;
		}
	}
}
?>
<link href='http://fonts.googleapis.com/css?family=Ubuntu' rel='stylesheet' type='text/css'>
<style type="text/css">
	body{
		margin:0;
		padding:0;
		font-family:'Ubuntu', sans-serif
	}
	#main{
		width:910px;
		height:620px;
		margin:30px auto
	}
	#first{
		width:400px;
		height:610px;
		box-shadow:0 0 0 1px rgba(14,41,57,0.12),0 2px 5px rgba(14,41,57,0.44),inset 0 -1px 2px rgba(14,41,57,0.15);
		float:left;
		padding:10px 50px 0;
		background:linear-gradient(#fff,#f2f6f9)
	}
	hr{
		border:0;
		border-top:1px solid #ccc;
		margin-bottom:30px
	}
	input{
		width:400px;
		padding:10px;
		margin-top:10px;
		margin-bottom:35px;
		border-radius:5px;
		border:1px solid #cbcbcb;
		box-shadow:inset 0 1px 2px rgba(0,0,0,0.18);
		font-size:16px
	}
	textarea{
		width:400px;
		height:100px;
		padding:10px;
		margin-top:10px;
		margin-bottom:35px;
		border-radius:5px;
		border:1px solid #cbcbcb;
		box-shadow:inset 0 1px 2px rgba(0,0,0,0.18);
		font-size:16px
	}
	input[type=submit]{
		background:linear-gradient(to bottom,#22abe9 5%,#36caf0 100%);
		box-shadow:inset 0 1px 0 0 #7bdcf4;
		border:1px solid #0F799E;
		color:#fff;
		font-size:19px;
		font-weight:700;
		cursor:pointer;
		text-shadow:0 1px 0 #13506D
	}
	input[type=submit]:hover{
		background:linear-gradient(to bottom,#36caf0 5%,#22abe9 100%)
	}
	label {
		font-size:17px
		float: left;
		text-align: right;
		margin-right: 15px;
		width: 300px;
	}
</style>
<div id="main">	
	<div id="first">
		<!-- register form -->
		<form method="post" action="register.php" name="registerform" class=".dark-matter">
			<h1> Registration Form </h1>
			<!-- the user name input field uses a HTML5 pattern check -->
			<label for="login_input_username">Username (2-64 characters)</label>
			<input id="login_input_username" class="login_input" type="text" pattern="[a-zA-Z0-9]{2,64}" name="user_name" required />
			<br />
			<!-- the email input field uses a HTML5 email type check -->
			<label for="login_input_email">Email Address</label>
			<input id="login_input_email" class="login_input" type="email" name="user_email" required />
			<br />

			<label for="login_input_password_new">Password (min. 6 characters)</label>
			<input id="login_input_password_new" class="login_input" type="password" name="user_password_new" pattern=".{6,}" required autocomplete="off" />
			<br />

			<label for="login_input_password_repeat">Repeat password</label>
			<input id="login_input_password_repeat" class="login_input" type="password" name="user_password_repeat" pattern=".{6,}" required autocomplete="off" />
			<br />
			<input type="submit" name="register" value="Register" />
			<a href="../index.php"> <input type="button" value="Back to Login Page"> </a>
		</form>
	</div>
</div>


<!-- backlink -->

